package com;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.carol.searchword.R;

import java.util.List;

public class Translator extends RecyclerView.Adapter<Translator.HoldView>
{
    private Context context;
    private List<String> listWords;

    public Translator(Context context, List<String> wordlist)
    {
        this.context = context;
        this.listWords = wordlist;
    }

    @Override
    public HoldView onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewtype)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.word_list, viewGroup, false);

        return new HoldView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HoldView viewHolder, int i)
    {
        viewHolder.words.setText(listWords.get(i));

    }

    @Override
    public int getItemCount() {
        return listWords.size();
    }

    public class HoldView extends RecyclerView.ViewHolder
    {
        public TextView words;

        public HoldView(@NonNull View item)
        {
            super(item);

            words = item.findViewById(R.id.words);
        }
    }

    public void addList(List<String> wordlist)
    {
        this.listWords = wordlist;
        notifyDataSetChanged();
    }
}
